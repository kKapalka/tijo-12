package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.CSVUtils;
import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    private static String csvFile = "C:\\Users\\student\\Downloads\\movies\\movies\\movies.csv";

    @RequestMapping(method = RequestMethod.GET,value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {
        return new MovieListDto(LoadMovies());
    }

    @RequestMapping(method = RequestMethod.POST,value = "/movies")
    public ResponseEntity createNewMovie(@RequestBody MovieDto movie){

        BufferedWriter writer=null;
        try{
            writer= new BufferedWriter(new FileWriter(csvFile, true));
            for (MovieDto tempmovie:LoadMovies()) {
                if(tempmovie.getMovieId()==movie.getMovieId()){
                    return new ResponseEntity<>(HttpStatus.CONFLICT);
                }
            }
        CSVUtils.writeLine(writer, movie.toListOfString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    private List<MovieDto> LoadMovies(){
        LOGGER.info("--- get movies");
        List<MovieDto> movies;
        movies = new ArrayList<>();

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] moviedata = line.split(cvsSplitBy);
                movies.add(new MovieDto(Integer.parseInt(moviedata[0].trim()),moviedata[1],Integer.parseInt(moviedata[2].trim()),moviedata[3]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return movies;
    }
}
